Contest (contest)
=================
Allows users to enter to win prizes via a random drawing, (sweepstakes).
-------------------------------------------------------------------------


Requirements
============
This module requires the following modules:
-------------------------------------------
- Block (core)
- Date (https://github.com/backdrop-contrib/date_basic)
- List (core)
- Text (core)
- Token (https://www.drupal.org/project/token)


Installation
============
1. Copy the 'contest' directory in to your Drupal modules directory.

2. Go to /admin/config/regional/settings to configure country and timezone.

3. Enable the module.

4. Flush the cache.

5. Go to /admin/config/contest and configure the contest settings.

6. Complete a profile for the contest host, (defaults to user 1). This
   information is used in the contest view, (particularly the rules fieldset).

7. Complete a profile for the contest sponsor, (if host and sponsor are the same
   you can skip this). Again, this information is used in the contest view
   (particularly the rules fieldset).


MAINTAINERS
===========
Current maintainers:
--------------------
- Bill Kelly (spot-the-cat) - https://github.com/spot-the-cat
